GEM := gem
BUNDLE := bundler
YARD := vendor/bundle/bin/yard

all: vendor build/vielfaltmenu-mealplan-calendar.gem

build/%.gem: $(wildcard *.gemspec) ## Build gemfile
	install -d $(dir $@)
	$(GEM) build $< -o $@

vendor: Gemfile $(wildcard *.gemspec) ## Install 3rd party libs
	$(BUNDLE) install

docs: lib/ ## Generate rdoc
	$(YARD) doc $^ -o $@

clean: ## Clean build, doc and vendor dir
	rm -rf build/
	rm -rf docs/
	rm -rf vendor/

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m %s\n", $$1, $$2}'

.PHONY: all clean help
