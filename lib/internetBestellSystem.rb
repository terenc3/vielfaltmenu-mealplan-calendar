require 'cgi'
require 'net/http'

module Vielfaltmenu
    class InternetBestellSystem
        ##
        # IBS api (Internet-Bestell-System)
        IBS_URI = 'https://ibs.vielfaltmenue.com/'

        # Retrieve order history html from today to -90 days
        #
        # @param [String] token user token
        # @param [String] iframe user iframe
        #
        # @return [String] order history table
        def self.order_history(token, iframe)
            from = CGI.escapeHTML((Date.today - 90).strftime('%a %b %d %Y'))
            to = CGI.escapeHTML(Date.today.strftime('%a %b %d %Y'))

            uri = URI.parse(IBS_URI + iframe + '/Account/Orderhistory?from=' + from + '&to=' + to + '&search=')

            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            response = http.get(uri.request_uri, {
                auth: token
            })

            case response
            when Net::HTTPClientError
                puts response.class.to_s + ': ' + response.body
                exit 1
            else
                response.body
            end
        end

        # Wrap line at 75 chars according to https://datatracker.ietf.org/doc/html/rfc5545#section-3.1
        #
        # @param [String] line
        # @return [String] multiline
        def self.wrap_line(line)
            line.chars.each_slice(75).map(&:join).join("\n ")
        end

        # Writes calendar to output
        #
        # @param [String] output Filename
        # @param [String] history Order history html

        # @param [Hash] opts options hash
        # @option opts [String] :location Event location
        # @option opts [String] :geo Event geo
        # @option opts [String] :attendee Entity which is added as attendee
        # @option opts [String] :organizer Entity which is added as organizer
        # @option opts [Boolean] :reminder Add reminder
        # @option opts [String] :reminder_text Reminder summary
        # @option opts [String] :reminder_days Reminder days before last order
        def self.generate(output, history, opts = {})
            stamp = Time.now.strftime('%Y%m%dT%H%M%S')
            table = Nokogiri::HTML.parse(history).css('#order-history-body')

            File.open(output, 'w') do |f|
                f.puts 'BEGIN:VCALENDAR'
                f.puts 'VERSION:2.0'
                f.puts 'PRODID:-//terenc3//NONSGML vielfaltmenu-mealplan-calendar//EN'
                table.search('tr').each do |tr|
                    day = Date.strptime(tr.xpath('td[2]/div/span').text, '%d.%m.%Y')
                    meal = tr.xpath('td[4]/div/span').text[14..-1]

                    f.puts 'BEGIN:VEVENT'
                    f.puts 'UID:' + SecureRandom.uuid
                    f.puts wrap_line('SUMMARY:' + meal)
                    f.puts 'CREATED:' + Date.strptime(tr.xpath('td[1]/div/span').text, '%d.%m.%Y').strftime('%Y%m%dT120000')
                    f.puts 'DTSTAMP:' + stamp
                    f.puts 'DTSTART:' + day.strftime('%Y%m%dT113000')
                    f.puts 'DTEND:' + day.strftime('%Y%m%dT122500')
                    if (opts[:location])
                        f.puts 'LOCATION:' + opts[:location]
                    end
                    if (opts[:geo])
                        f.puts 'GEO:' + opts[:geo]
                    end
                    f.puts 'STATUS:' + (tr.xpath('td[3]/div/span').text == '-1' ? 'CANCELLED' : 'CONFIRMED')
                    if (opts[:organizer])
                        f.puts 'ORGANIZER;' + opts[:organizer]
                        f.puts wrap_line('ATTENDEE;CUTYPE=ROOM;PARTSTAT=ACCEPTED;ROLE=CHAIR;' + opts[:organizer])
                    end
                    if (opts[:attendee])
                        f.puts wrap_line('ATTENDEE;CUTYPE=INDIVIDUAL;PARTSTAT=ACCEPTED;ROLE=REQ-PARTICIPANT;' + opts[:attendee])
                    end
                    f.puts 'END:VEVENT'
                end

                if (opts[:reminder])
                    f.puts 'BEGIN:VEVENT'
                    f.puts 'UID:' + SecureRandom.uuid
                    f.puts 'SUMMARY:' + opts[:reminder_text]
                    f.puts 'DTSTAMP:' + stamp
                    f.puts 'DTSTART:' + (Date.strptime(table.xpath('tr[1]/td[2]/div/span').text, '%d.%m.%Y') - opts[:reminder_days]).strftime('%Y%m%dT180000')
                    f.puts 'END:VEVENT'
                    f.puts 'END:VCALENDAR'
                end

                #f.puts 'BEGIN:VTODO'
                #f.puts 'UID:' + SecureRandom.uuid
                #f.puts 'SUMMARY:Essen bestellen'
                #f.puts 'URL:https://vielfaltmenue.com/'
                #f.puts 'DTSTAMP:' + stamp
                #f.puts 'DUE;VALUE=DATE:' + (Date.strptime(table.xpath('tr[1]/td[2]/div/span').text, '%d.%m.%Y') -7).strftime('%Y%m%d')
                #f.puts 'END:VTODO'
                #f.puts 'END:VCALENDAR'
            end
        end
    end
end