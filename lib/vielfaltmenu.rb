require 'net/http'
require 'json'

module Vielfaltmenu
    class Vielfaltmenu
        # Auth api
        AUTH_URI = 'https://api.vielfaltmenue.com'

        # Login
        #
        # @param [String] username vielfaltmenu username
        # @param [String] password vielfaltmenu password
        #
        # @return [Hash] various user info
        def self.login(username, password)
            response = Net::HTTP.post(URI(AUTH_URI + '/frontend/login'), URI.encode_www_form({
                'username' => username,
                'password' => password
            }), {
                content_type: 'application/x-www-form-urlencoded'
            })

            case response
            when Net::HTTPClientError
                puts response.class.to_s + ': ' + response.body
                exit 1
            else
                JSON.parse(response.body)
            end
        end
    end
end