Gem::Specification.new do |s|
  s.name        = "vielfaltmenu-mealplan-calendar"
  s.version     = "1.0.0"
  s.summary     = "Vielfaltmenu mealplan calender"
  s.description = "Parse the vielfaltmenu order history to create a subscribable calender"
  s.authors     = ["Benjamin Kahlau"]
  s.email       = "terenc3@roanapur.de"
  s.files       = Dir.glob("{exe,lib}/**/*")
  s.bindir      = "exe"
  s.executables << "vielfaltmenu-mealplan-calendar"
  s.homepage    = "https://codeberg.org/terenc3/vielfaltmenu-mealplan-calendar"
  s.license     = "GPL-3.0"

  s.add_runtime_dependency 'nokogiri', '~> 1.14', '>= 1.14.3'
  s.add_runtime_dependency 'dotenv', '~> 2.8.1', '>= 2.8.1'
  s.add_development_dependency 'yard', '~> 0.9.34', '>= 0.9.34'
end
