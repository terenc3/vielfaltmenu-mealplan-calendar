# Vielfaltmenu meal calendar
> Parse the vielfaltmenu order history to create a subscribable calender
> 
> https://vielfaltmenue.com

## Dev
ruby ruby-dev ruby-bundler

## Build
```shell
apt install ruby ruby-dev ruby-bundler
make
```

## Install
```shell
gem install --local build/vielfaltmenu-mealplan-calendar.gem
```

## Setup systemd timer
```shell
cp systemd/* /etc/systemd/system/
systemctl daemon-reload

# Create common environment vars (e.g. reminder, location)
vim /etc/vielfaltmenu-mealplan-calendar/common.conf

# Create user specific environment vars (e.g. username, password)
vim /etc/vielfaltmenu-mealplan-calendar/alice.conf

# Modify Wants field in target to define user environments
# Wants=vielfaltmenu-mealplan-calendar@alice.service vielfaltmenu-mealplan-calendar@bob.service
vim /etc/systemd/system/vielfaltmenu-mealplan-calendar.target

# Single run for user alice (testing)
sudo systemctl start vielfaltmenu-mealplan-calendar@alice.service
```

## Configuration
_Note:_ Environment variables are always strings but are getting parsed to mentioned type.

| Name              | Type    | Description                                                                                        |
|-------------------|---------|----------------------------------------------------------------------------------------------------|
| VFM_USERNAME      | string  |                                                                                                    |  
| VFM_PASSWORD      | string  |                                                                                                    |
| VFM_OUTPUT        | string  | Output file e.g. /var/www/html/mealplan.ics                                                        |
| VFM_LOCATION      | string  | (optional) [VEVENT LOCATION](https://icalendar.org/iCalendar-RFC-5545/3-8-1-7-location.html)       |
| VFM_GEO           | string  | (optional) [VEVENT GEO](https://icalendar.org/iCalendar-RFC-5545/3-8-1-6-geographic-position.html) |
| VFM_ATTENDEE      | string  | (optional) Attendee string e.g. CN=Max:mailto:alice@example.com                                      |
| VFM_ORGANIZER     | string  | (optional) Organizer string e.g. CN=VielfaltMenu:mailto:noreply@example.com                        |
| VFM_REMINDER      | boolean | (optional)                                                                                         |
| VFM_REMINDER_DAYS | number  | (optional)                                                                                         |
| VFM_REMINDER_TEXT | string  | (optional)                                                                                         |

## Example
```
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//terenc3//NONSGML vielfaltmenu-mealplan-calendar//EN
BEGIN:VEVENT
UID:c024ffe9-09e0-4737-a2f5-d1e6ed10a711
SUMMARY:Hühnerfrikassee mit Erbsen | Möhren und Vollkornreis, dazu Obst (G)
CREATED:20230122T120000
DTSTAMP:20230421T103735
DTSTART:20230126T113000
DTEND:20230126T122500
LOCATION:Street Name 1 // Building, ZipCode City
STATUS:CONFIRMED
ATTENDEE;CUTYPE=INDIVIDUAL;PARTSTAT=ACCEPTED;ROLE=REQ-PARTICIPANT;CN=Alice
 :mailto:alice@example.com
END:VEVENT
BEGIN:VEVENT
UID:d130a241-0382-4a2d-a8fe-e8e9fb25dded
SUMMARY:Order meals
DTSTAMP:20230421T103735
DTSTART:20230421T180000
END:VEVENT
END:VCALENDAR
```